const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');
const settings = {
  dimensions: [ 2048, 2048 ]
};

/* Nice seeds
  770638
*/
random.setSeed(random.getRandomSeed())
console.log(random.getSeed())

const margin = 200;
const palette = random.pick(palettes);

const sketch = () => {
  let points = createGrid().filter(() => random.value() > 0.7);
  
  return ({ context, width, height }) => {
    context.fillStyle = palette[2];
    context.fillRect(0, 0, width, height);

    points.forEach(drawPoints);

    function drawPoints(data){
      const {
        color,
        position,
        rotation,
        radius
      } = data

      const [u, v] = position
      let x = lerp(margin, width - margin, u);
      let y = lerp(margin, height - margin, v);

      context.save()
      context.fillStyle = color;
      context.font = `${radius}px "Arial"`;
      context.translate(x, y);
      context.rotate(rotation)
      context.fillText('>', 0, 0);
      context.restore();
    }

  };
};

canvasSketch(sketch, settings);

function createGrid(){
  let points = [];
  let count = 100;
  for (let x = 0; x < count; x++){
    for (let y = 0; y < count; y++){
      let u = count <= 1 ? 0.5 : x / (count - 1);
      let v = count <= 1 ? 0.5 : y / (count - 1);
      let radius = random.noise2D(u, v);
      points.push({
        color: random.pick(palette),
        position: [u, v],
        radius: Math.max(0,radius)*100 + 50,
        rotation: random.noise1D(u)
      });
    }
  }
  return points;
}