const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');
const settings = {
  dimensions: [ 2048, 2048 ]
};

const margin = 200;
const palette = random.pick(palettes);
random.setSeed(60);

const sketch = () => {
  let points = createGrid().filter(() => random.value() > 0.3);
  
  return ({ context, width, height }) => {
    context.fillStyle = palette[2];
    context.fillRect(0, 0, width, height);

    points.forEach(drawPoints);

    function drawPoints(data){
      const {
        color,
        position,
        radius
      } = data

      const [u, v] = position
      let x = lerp(margin, width - margin, u);
      let y = lerp(margin, height - margin, v);

      context.beginPath();
      context.arc(x,y, radius, 0, Math.PI * 2, false)
      context.strokeStyle = 'white';
      context.fillStyle = color;
      context.lineWidth = 5;
      //context.stroke();
      context.fill();
    }

  };
};

canvasSketch(sketch, settings);

function createGrid(){
  let points = [];
  let count = 50;
  for (let x = 0; x < count; x++){
    for (let y = 0; y < count; y++){
      let u = count <= 1 ? 0.5 : x / (count - 1);
      let v = count <= 1 ? 0.5 : y / (count - 1);
      points.push({
        color: random.pick(palette.slice(0,2)),
        position: [u, v],
        radius: Math.max(0,random.gaussian())*20
      });
    }
  }
  return points;
}